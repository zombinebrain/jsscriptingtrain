function translatePigLatin(str) {
    let regex = /^[^aeiou]+/;
    let noVowelSearch = str.match(regex);
    if(noVowelSearch !== null) {
        return str.replace(regex, '').concat(noVowelSearch) + 'ay';
    } else {
        return str + 'way';
    }
}

console.log(translatePigLatin("glove"));
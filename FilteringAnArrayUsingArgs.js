function  destroyer(arr,...args) {
    return arr.filter(item => {
        return !args.includes(item);
    });
}

console.log(destroyer([1,2,3,4,2,3,5,2,7,3],2,3));
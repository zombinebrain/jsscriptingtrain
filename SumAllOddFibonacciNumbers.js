function sumFibs(num) {
    let a,b = 1, c = 0, arr = [];
    for(let i = 0; c <= num; i++) {
        a = b;
        b = c;
        c = a + b;
        arr.push(c);
    }
    arr.pop();
    return arr.filter(item => item % 2 !== 0).reduce((a,b) => a + b);
}

console.log(sumFibs(1000));
function myReplace(str, before, after) {
    if(/^[A-Z]/.test(before)) {
        after = after[0].toUpperCase() + after.substr(1);
    }
    return str.replace(before,after);
}

console.log(myReplace('A quick Brown dog', 'Brown', 'black'));
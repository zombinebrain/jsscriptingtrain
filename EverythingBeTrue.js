function truthCheck(collection, pre) {
    let arr = [];
    for(let i = 0; i < collection.length; i++) {
        if(collection[i].hasOwnProperty(pre) === true && Boolean(collection[i][pre]) == true) {
            arr.push(collection[i]);
        }
    }
    return arr.length == collection.length;
}

console.log(truthCheck([{"user": "Tinky-Winky", "sex": "male"}, {"user": "Dipsy", "sex": "male"}, {"user": "Laa-Laa", "sex": "female"}, {"user": "Po", "sex": "female"}], "sex"));
function spinalCase(str) {
    return str.split(/\W|_|(?=[A-Z])/).join('-').toLowerCase();
}

console.log(spinalCase("AllThe-small Things"));


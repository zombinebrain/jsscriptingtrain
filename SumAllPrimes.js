function sumPrimes(num) {
    function primes(num) {
        if(num < 2) return false;
        for (let i = 2; i < num; i++) {
            if(num % i == 0)
                return false;
        }
        return true;
    };
    let sum = 0;
    for(let i = 0; i <= num; i++) {
        if(primes(i)) {
            sum += i;
        }
    }
    return sum;
}

console.log(sumPrimes(10));